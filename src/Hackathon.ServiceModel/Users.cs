﻿//
//  Users.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using System.Collections.Generic;
using Hackathon.ServiceModel.Types;
using System.Linq;

namespace Hackathon.ServiceModel
{
	[Route ("/users/{id}", "GET")]
	public class GetUser : IReturn<GetUserResponse>
	{
		public int Id { get; set; }
	}

	[Route ("/user/password", "POST")]
	public class ChangePassword : IReturnVoid
	{
		public string OldPassword { get; set; }

		public string NewPassword { get; set; }
	}

	[Route ("/users/createresetcode", "POST")]
	public class CreateResetCode : IReturnVoid
	{
		public string MailAddress { get; set; }
	}

	[Route ("/users/resetpassword", "POST")]
	public class ResetPassword : IReturnVoid
	{
		public string MailAddress { get; set; }

		public string ResetCode { get; set; }

		public string NewPassword { get; set; }
	}

	[Route ("/users/{userId}/challenges/{challengeId}", "GET")]
	public class GetUserChallenge : IReturn<UserChallengeResponse>
	{
		public int UserId { get; set; }

		public int ChallengeId{ get; set; }
	}

	[Route ("/users/{userId}/challenges/{challengeId}/points", "GET")]
	public class GetUserChallengePoints : IReturn<PointResponse>
	{
		public int UserId { get; set; }

		public int ChallengeId { get; set; }

		public bool ForceCalculation { get; set; }
	}

	[Route ("/users/{userId}/challenges/{challengeId}/pointsifsolvednow", "GET")]
	public class GetUserChallengePointsIfSolvedNow : IReturn<PointResponse>
	{
		public int UserId { get; set; }

		public int ChallengeId{ get; set; }
	}

	[Route ("/user", "GET")]
	public class GetLoggedInUser : IReturn<GetUserResponse>
	{
	}

	[Route ("/user/events", "GET")]
	public class GetLoggedInUserEvents : IReturn<IList<int>>
	{
	}

	[Route ("/user/", "PUT")]
	public class ChangeUser : IReturnVoid
	{
		public String FirstName { get; set; }

		public String LastName { get; set; }

		public String Avatar { get; set; }
	}

	public class GetUserResponse
	{
		public int Id { get; set; }

		public string UserName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Avatar { get; set; }

		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType () != typeof(GetUserResponse))
				return false;
			GetUserResponse other = (GetUserResponse)obj;
			return Id == other.Id && UserName == other.UserName && FirstName == other.FirstName && LastName == other.LastName && Avatar == other.Avatar;
		}

		public override int GetHashCode ()
		{
			unchecked {
				return Id.GetHashCode () ^ (UserName != null ? UserName.GetHashCode () : 0) ^ (FirstName != null ? FirstName.GetHashCode () : 0) ^ (LastName != null ? LastName.GetHashCode () : 0) ^ (Avatar != null ? Avatar.GetHashCode () : 0);
			}
		}
	}

	public class UserChallengeResponse
	{
		public String AccessDate { get; set; }

		public String SolvedDate { get; set; }

		public int TryCount { get; set; }

		public List<HintAccessResponse> HintAccess { get; set; }

		public int BasePoints { get; set; }

		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType () != typeof(UserChallengeResponse))
				return false;
			UserChallengeResponse other = (UserChallengeResponse)obj;
			return AccessDate == other.AccessDate && SolvedDate == other.SolvedDate && TryCount == other.TryCount && (HintAccess == other.HintAccess || HintAccess.SequenceEqual (other.HintAccess));
		}

		public override int GetHashCode ()
		{
			unchecked {
				return (AccessDate != null ? AccessDate.GetHashCode () : 0) ^ (SolvedDate != null ? SolvedDate.GetHashCode () : 0) ^ TryCount.GetHashCode () ^ (HintAccess != null ? HintAccess.GetHashCode () : 0);
			}
		}
	}

	public class HintAccessResponse
	{
		public int HintId { get; set; }

		public String AccessTime { get; set; }

		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType () != typeof(HintAccessResponse))
				return false;
			HintAccessResponse other = (HintAccessResponse)obj;
			return HintId == other.HintId && AccessTime == other.AccessTime;
		}

		public override int GetHashCode ()
		{
			unchecked {
				return HintId.GetHashCode () ^ (AccessTime != null ? AccessTime.GetHashCode () : 0);
			}
		}
	}
}

