﻿//
//  User.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack.DataAnnotations;
using System.Collections.Generic;

namespace Hackathon.ServiceModel.Types
{
	public class User
	{
		[AutoIncrement]
		public int Id { get; set; }

		[Index (true)]
		public string UserName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		[StringLength (int.MaxValue)] // Will result in a datatype of LONGTEXT in MySQL
		public string Avatar { get; set; }

		public String Salt { get; set; }

		//TODO: Do some validation, that only real E-Mail addresses can be inserted here
		[Index (true)]
		public String MailAddress { get; set; }

		/// <summary>
		/// The password is stored as the base64 encoding of SHA-256(Salt | password)
		/// </summary>
		public string Password { get; set; }

		public string ResetCode { get; set; }

		public DateTime ResetCodeNotAfter { get; set; }

		public DateTime? PasswordChangeDate { get; set; }

		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType () != typeof(User))
				return false;
			User other = (User)obj;
			return Id == other.Id && UserName == other.UserName && FirstName == other.FirstName && LastName == other.LastName && Salt == other.Salt && MailAddress == other.MailAddress && Password == other.Password && ResetCode == other.ResetCode && ResetCodeNotAfter == other.ResetCodeNotAfter && PasswordChangeDate == other.PasswordChangeDate;
		}


		public override int GetHashCode ()
		{
			unchecked {
				return Id.GetHashCode () ^ (UserName != null ? UserName.GetHashCode () : 0) ^ (FirstName != null ? FirstName.GetHashCode () : 0) ^ (LastName != null ? LastName.GetHashCode () : 0) ^ (Salt != null ? Salt.GetHashCode () : 0) ^ (MailAddress != null ? MailAddress.GetHashCode () : 0) ^ (Password != null ? Password.GetHashCode () : 0) ^ (ResetCode != null ? ResetCode.GetHashCode () : 0) ^ ResetCodeNotAfter.GetHashCode () ^ PasswordChangeDate.GetHashCode ();
			}
		}
		
		
	}
}

