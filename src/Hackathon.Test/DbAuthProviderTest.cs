﻿//
//  DbAuthProviderTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using NUnit.Framework;
using System;
using System.Linq;
using ServiceStack;
using ServiceStack.Testing;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel.Types;
using Hackathon.ServiceModel;
using Funq;
using ServiceStack.Auth;
using ServiceStack.Caching;
using System.Threading;
using System.Collections.Generic;

namespace Hackathon.ServiceInterface.Test
{
	/*public class AuthAppHost : BasicAppHost
	{
		private readonly Action<Container> configureFn;

		public AuthAppHost () : base (typeof(UsersService).Assembly)
		{
		}

		public override void Configure (Container container)
		{
//			Plugins.Add (new AuthFeature (() => new CustomUserSession (),
//				GetAuthProviders (), "~/" + AuthTests.LoginUrl) {
//				RegisterPlugins = { new WebSudoFeature () }
//			});

			container.Register<IDbConnectionFactory> (c =>
				new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
			);

			using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
				db.DropAndCreateTable<User> ();

				db.Insert (new User {
					UserName = "foobar",
					FirstName = "Foo",
					LastName = "bar",
					Avatar = null,
					//pw=123456, created with echo -n "foobar123456"|sha256sum|base64
					Password = @"NTdiY2FkYjYwOGRiMjhhM2JjYWNhYTdhMzkzODg3YTkwZjAxMDkyMTBhYWEzNjg2ODAxYjZlOWY4
OTU2OGRmYiAgLQo="
				});
			}
			Plugins.Add (new AuthFeature (() => new AuthUserSession (),
				new IAuthProvider[] { 
					new DbAuthProvider ()
				}));
			
			container.Register (new MemoryCacheClient ());
			var userRep = new InMemoryAuthRepository ();
			container.Register<IAuthRepository> (userRep);

		}
*/

	public class AppHost : AppSelfHostBase
	{
		public AppHost ()
			: base ("HttpListener Self-Host", typeof(UsersService).Assembly)
		{
		}

		public override void Configure (Funq.Container container)
		{
		}
	}

	public class AuthAppHost : AppSelfHostBase
	{

		public AuthAppHost () : base ("Validation Tests", typeof(UsersService).Assembly)
		{
		}

		private InMemoryAuthRepository userRep;

		public override void Configure (Container container)
		{
			//SetConfig (new HostConfig { WebHostUrl = webHostUrl, DebugMode = true });

			container.Register<IDbConnectionFactory> (c =>
				new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
			);

			using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
				db.DropAndCreateTable<User> ();

				db.Insert (new User {
					UserName = "foobar",
					FirstName = "Foo",
					LastName = "bar",
					Avatar = null,
					//pw=123456, created with echo -n "foobar123456"|sha256sum|base64
					Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
				});
			}

//			Plugins.Add (new AuthFeature (() => new AuthUserSession (),
//				new IAuthProvider[] { 
//					new DbAuthProvider ()
//				}));
//
//			container.Register (new MemoryCacheClient ());
//			userRep = new InMemoryAuthRepository ();
//			container.Register<IAuthRepository> (userRep);

		}

		protected override void Dispose (bool disposing)
		{
			// Needed so that when the derived class tests run the same users can be added again.
			userRep.Clear ();
			base.Dispose (disposing);
		}
	}

	[TestFixture ()]
	public class DbAuthProviderTest
	{
		ServiceStackHost appHost;
		string appHostUrl = "http://127.0.0.1:8989/";

		[TestFixtureSetUp]
		public void OnTestFixtureSetUp ()
		{
			//appHost = new AuthAppHost ();
			appHost = new AppHost ();
			appHost.Init ();
			appHost.Start ("http://*:8989/");//appHostUrl);
		}

		[TestFixtureTearDown]
		public void OnTestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test ()]
		[Ignore ("Not working")]
		public void AuthWithGoodPasswordTest ()
		{
			Thread.Sleep (60000);
			//	var client = new JsonServiceClient (appHostUrl);
//			var authResponse = client.Send (new Authenticate () {
//				//	provider = DbAuthProvider.Name,
//				UserName = "foobar",
//				Password = "123456"
//			});
			//	var response = client.Send<IList<FindUsersResponse>> (new FindUsersResponse ());
			//		Console.WriteLine (response);
		}
	}
}
