﻿//
//  UsersServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using NUnit.Framework;
using System;
using System.Linq;
using ServiceStack;
using ServiceStack.Testing;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel.Types;
using Hackathon.ServiceModel;
using ServiceStack.Auth;
using ServiceStack.Caching;
using System.Collections.Generic;
using Hackathon.ServiceModel.Auth;
using System.Net;
using Moq;
using Hackathon.ServiceInterface.Notification;
using Hackathon.ServiceInterface.Points;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture ()]
	public class UsersServiceTest
	{
		static Mock<INotifyUser> notifierMock = new Mock<INotifyUser> ();

		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(UsersService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});

				Plugins.Add (new AuthFeature (
					() => new AuthUserSession (),
					new IAuthProvider[] { new DbAuthProvider () }) {
					// Do not redirect when trying to access a service with an AuthenticatedAttribute (only valid for content type HTML), just return the error code instead
					HtmlRedirect = null,
				}
				);

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<EventChallengeSets> ();
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<Hint> ();
					db.DropAndCreateTable<ChallengeAccess> ();
					db.DropAndCreateTable<ChallengeSolution> ();
					db.DropAndCreateTable<HintAccess> ();

					db.Insert (new Event{ Id = 1 });
					db.Insert (new Event{ Id = 2 });
					db.Insert (new Event{ Id = 3 });

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = @"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9TXL0Y4OHwAAAABJRU5ErkJggg==",
						MailAddress = "foobar@mailinator.com",
						Salt = "foobar",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

					db.Insert (new User {
						Id = 2,
						UserName = "doe",
						FirstName = "John",
						LastName = "Doe",
						Avatar = @"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9999999999AAAABJRU5ErkJggg==",
					});

					db.Insert (new User {
						Id = 3,
						UserName = "blubb",
					});

					db.Insert (new User {
						Id = 4,
						UserName = "bla",
					});

					db.Insert (new Challenge {
						Id = 1,
						Title = "A challenge",
						Solution = "A solution",
						Description = "Info about the challenge",
						BasePoints = 100,
					});

					db.Insert (new ChallengeSet { Id = 1, Challenges = new List<int>{ 1 } });
					db.Insert (new Event { Id = 1 });
					db.Insert (new EventChallengeSets {
						ChallengeSetId = 1,
						EventId = 1,
						StartDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0)),
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0))
					});

					db.Insert (new Hint {
						ChallengeId = 1,
						Text = "Hint #1"
					});

					db.Insert (new Hint {
						ChallengeId = 1,
						Text = "Hint #2"
					});

					db.Insert (new EventUsers { EventId = 1, UserId = 1 });
					db.Insert (new EventUsers { EventId = 1, UserId = 2 });
					db.Insert (new EventUsers { EventId = 2, UserId = 1 });
					db.Insert (new EventUsers { EventId = 1, UserId = 3 });
				}

				//Mock the user notifier
				container.Register<INotifyUser> (notifierMock.Object);
			}
		}

		private ServiceStackHost appHost;

		[SetUp]
		public void SetUpUsersServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Console.WriteLine (e);
				Assert.Fail (e.ToString ());
			}
		}

		[TearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}

		[Test ()]
		public void GetUserTest ()
		{
			GetUserResponse expected = new GetUserResponse {
				Id = 2,
				UserName = "doe",
				FirstName = "John",
				LastName = "Doe",
				Avatar = @"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9999999999AAAABJRU5ErkJggg==",
			};

			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var user = service.Get (new GetUser { Id = 2 });
				Assert.That (user, Is.EqualTo (expected));
				//Try to get a user from another event
				service.Get (new GetUser { Id = 3 });
			}
		}

		[Test ()]
		public void GetLoggerInUserTest ()
		{
			GetUserResponse expected = new GetUserResponse {
				Id = 1,
				UserName = "foobar",
				FirstName = "Foo",
				LastName = "bar",
				Avatar = @"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9TXL0Y4OHwAAAABJRU5ErkJggg=="
			};

			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var user = service.Get (new GetLoggedInUser ());
				Assert.That (user, Is.EqualTo (expected));
			}
		}

		[Test ()]
		public void GetLoggerInUserEventsTest ()
		{
			List<int> expected = new List<int>{ 1, 2 };

			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var events = service.Get (new GetLoggedInUserEvents ());
				Assert.That (expected.SequenceEqual (events));
			}
		}

		[Test ()]
		public void GetUserNoSharedEventTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetUser { Id = 4 });
					Assert.Fail ("This should not work");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void ChangePasswordTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				string userAuthName = "foobar@mailinator.com";
				string oldPassword = "123456";
				string newPassword = "abcdef";
				var auth = new DbAuthProvider ();
				Assert.That (auth.TryAuthenticate (service, userAuthName, oldPassword), "The old password should be valid before we are changing it.");
				service.Post (new ChangePassword () {
					OldPassword = oldPassword,
					NewPassword = newPassword
				});
				Assert.That (!auth.TryAuthenticate (service, userAuthName, oldPassword), "The old password should not work anymore.");
				Assert.That (auth.TryAuthenticate (service, userAuthName, newPassword), "The new password should work now.");

				//Check if the PasswordChangeDate has been updated
				using (var db = service.TryResolve<IDbConnectionFactory> ().Open ()) {
					User user = db.Select<User> ().Where (u => u.Id == 1).First ();
					Assert.That (DateTime.Now.Subtract (user.PasswordChangeDate.Value).TotalSeconds, Is.LessThanOrEqualTo (1), "The PasswordChangeDate must have been set.");
				}
			}
		}

		[Test ()]
		public void ChangePasswordBadOldPasswordTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				using (var db = service.TryResolve<IDbConnectionFactory> ().Open ()) {
					User user = db.Select<User> ().Where (u => u.Id == 1).First ();
					DateTime? oldPasswordChangeDate = user.PasswordChangeDate;
				
					string badOldPassword = "123456foo";
					try {
						service.Post (new ChangePassword () {
							OldPassword = badOldPassword,
							NewPassword = "12345678",
						});
						Assert.Fail ("Changing the password without the correct current pasword must not work.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Unauthorized));
					}
					user = db.Select<User> ().Where (u => u.Id == 1).First ();
					DateTime? newPasswordChangeDate = user.PasswordChangeDate;
					Assert.That (oldPasswordChangeDate, Is.EqualTo (newPasswordChangeDate), "The PasswordChangeDate must not change if the password change failed.");
				}
			}
		}

		[Test ()]
		public void ChangeAvatarTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var oldUser = service.Db.Single<User> (u => u.Id == 1);
				String newAvatar = @"data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
							123123123123123lAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							abcabcabcabcabcJRU5ErkJggg==";
				service.Put (new ChangeUser{ Avatar = newAvatar });
				var newUser = service.Db.Single<User> (u => u.Id == 1);
				Assert.That (newUser.Avatar, Is.EqualTo (newAvatar));
				//verify that other values of the user are unchanged
				Assert.That (newUser.FirstName, Is.EqualTo (oldUser.FirstName));
				Assert.That (newUser.LastName, Is.EqualTo (oldUser.LastName));
				Assert.That (newUser.UserName, Is.EqualTo (oldUser.UserName));
				Assert.That (newUser.Salt, Is.EqualTo (oldUser.Salt));
				Assert.That (newUser.Password, Is.EqualTo (oldUser.Password));
				Assert.That (newUser.MailAddress, Is.EqualTo (oldUser.MailAddress));
			}
		}

		[Test ()]
		public void ChangeAvatarNoAvatarTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var oldUser = service.Db.Single<User> (u => u.Id == 1);
				string newAvatar = "";
				service.Put (new ChangeUser{ Avatar = newAvatar });
				var newUser = service.Db.Single<User> (u => u.Id == 1);
				Assert.That (newUser.Avatar, Is.Null);
				//verify that other values of the user are unchanged
				Assert.That (newUser.FirstName, Is.EqualTo (oldUser.FirstName));
				Assert.That (newUser.LastName, Is.EqualTo (oldUser.LastName));
				Assert.That (newUser.UserName, Is.EqualTo (oldUser.UserName));
				Assert.That (newUser.Salt, Is.EqualTo (oldUser.Salt));
				Assert.That (newUser.Password, Is.EqualTo (oldUser.Password));
				Assert.That (newUser.MailAddress, Is.EqualTo (oldUser.MailAddress));
			}
		}

		[Test ()]
		public void ChangeUserFirstLastNameTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var oldUser = service.Db.Single<User> (u => u.Id == 1);
				service.Put (new ChangeUser{ FirstName = "Jane", LastName = "Doe" });
				var newUser = service.Db.Single<User> (u => u.Id == 1);
				Assert.That (newUser.FirstName, Is.EqualTo ("Jane"));
				Assert.That (newUser.LastName, Is.EqualTo ("Doe"));

				//verify that other values of the user are unchanged
				Assert.That (newUser.Avatar.SequenceEqual (oldUser.Avatar));
				Assert.That (newUser.UserName, Is.EqualTo (oldUser.UserName));
				Assert.That (newUser.Salt, Is.EqualTo (oldUser.Salt));
				Assert.That (newUser.Password, Is.EqualTo (oldUser.Password));
				Assert.That (newUser.MailAddress, Is.EqualTo (oldUser.MailAddress));
			}
		}

		[Test ()]
		public void ChangeUserAllValuesTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var oldUser = service.Db.Single<User> (u => u.Id == 1);
				string newAvatar = @"data:image/png;base64, 0000000000oAAAANSUhEUgAAAAUA
							AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
							9TXL0Y4OHwAAAABJRU5ErkJggg==";
				service.Put (new ChangeUser {
					FirstName = "Jane",
					LastName = "Doe",
					Avatar = newAvatar
				});
				var newUser = service.Db.Single<User> (u => u.Id == 1);
				Assert.That (newUser.FirstName, Is.EqualTo ("Jane"));
				Assert.That (newUser.LastName, Is.EqualTo ("Doe"));
				Assert.That (newUser.Avatar, Is.EqualTo (newAvatar));

				//verify that other values of the user are unchanged
				Assert.That (newUser.UserName, Is.EqualTo (oldUser.UserName));
				Assert.That (newUser.Salt, Is.EqualTo (oldUser.Salt));
				Assert.That (newUser.Password, Is.EqualTo (oldUser.Password));
				Assert.That (newUser.MailAddress, Is.EqualTo (oldUser.MailAddress));
			}
		}

		[Test ()]
		public void ChangeUserFirstLastNameBadValuesTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				try {
					service.Put (new ChangeUser{ FirstName = "" });
					Assert.Fail ("An empty value for the FirstName should not be allowed.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}

				try {
					service.Put (new ChangeUser{ LastName = "" });
					Assert.Fail ("An empty value for the LastName should not be allowed.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void CreateResetCodeTest ()
		{
			using (var service = new UsersService ()) {
				using (var db = service.TryResolve<IDbConnectionFactory> ().Open ()) {
					User user = db.Select<User> ().Where (u => u.Id == 1).First ();
					string oldResetCode = user.ResetCode;
					service.Post (new CreateResetCode (){ MailAddress = "foobar@mailinator.com" });
					//reload the user from ther database
					user = db.Select<User> ().Where (u => u.MailAddress == "foobar@mailinator.com").First ();
					Assert.That (oldResetCode, Is.Not.EqualTo (user.ResetCode), "The resetcode should be changed now");
					Assert.That (user.ResetCodeNotAfter, Is.GreaterThan (DateTime.Now), "The validity of the reset code has to be in the future");
					//Verify that the user has been notified
					notifierMock.Verify (m => m.Notify (It.IsAny<String> (), It.IsAny <String> (), It.Is<User> (u => u.MailAddress == user.MailAddress)));
				}
			}
		}

		[Test ()]
		public void CreateResetCodeUnknkownMailAddressTest ()
		{
			using (var service = new UsersService ()) {
				service.Post (new CreateResetCode (){ MailAddress = "unknown@mailinator.com" });
				//This should silently fail and cause no error
			}
		}

		[Test ()]
		public void ResetPasswordTest ()
		{
			using (var service = new UsersService ()) {
				using (var db = service.TryResolve<IDbConnectionFactory> ().Open ()) {
					User user = db.Select<User> ().Where (u => u.MailAddress == "foobar@mailinator.com").First ();
					string oldResetCode = user.ResetCode;
					service.Post (new CreateResetCode (){ MailAddress = "foobar@mailinator.com" });
					//reload the user from ther database
					user = db.Select<User> ().Where (u => u.MailAddress == "foobar@mailinator.com").First ();
					string resetCode = user.ResetCode;

					//Try using a bad reset code first
					try {
						service.Post (new ResetPassword () {
							MailAddress = user.MailAddress,
							ResetCode = resetCode + "foo",
							NewPassword = "abcdef"
						});	
						Assert.Fail ("Using a bad reset code should not work");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
					}

					service.Post (new ResetPassword () {
						MailAddress = user.MailAddress,
						ResetCode = resetCode,
						NewPassword = "abcdef"
					});
					//reload the user from ther database
					user = db.Select<User> ().Where (u => u.Id == 1).First ();
					Assert.That (user.ResetCode, Is.Null, "Reset code should be removed from the database upon usage");
					Assert.That (DbAuthProvider.HashPassword (user.Salt, "abcdef"), Is.EqualTo (user.Password), "The password should be set to 'abcdef'");
					//Check if the PasswordChangeDate has been updated
					Assert.That (DateTime.Now.Subtract (user.PasswordChangeDate.Value).TotalSeconds, Is.LessThanOrEqualTo (1), "The PasswordChangeDate must have been set.");
				}
			}
		}

		[Test ()]
		public void ResetPasswordWithUnknownMailAddressTest ()
		{
			using (var service = new UsersService ()) {
				try {
					service.Post (new ResetPassword () {
						MailAddress = "unknown@mailinator.com",
						ResetCode = "foo",
						NewPassword = "bar"
					});	
					Assert.Fail ("Using an unknown mail address should not work");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
					Assert.That (e.Message, Is.EqualTo ("Password reset failed."), "Error message on a bad e-mail address should be a generic one.");
				}
			}
		}

		[Test ()]
		public void GetUserChallengeTest ()
		{
			int challengeId = 1;
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				var userChallenge = service.Get (new GetUserChallenge {
					UserId = 1,
					ChallengeId = challengeId
				});
				Assert.That (userChallenge.AccessDate, Is.Null);
				Assert.That (userChallenge.HintAccess, Is.Null);
				Assert.That (userChallenge.SolvedDate, Is.Null);
				Assert.That (userChallenge.TryCount, Is.EqualTo (0));
				Assert.That (userChallenge.BasePoints, Is.EqualTo (100));

				//Access the challenge
				using (var challengeService = new ChallengesService { Request = new MockHttpRequest () }) {
					challengeService.Get (new GetChallengeDescription{ Id = challengeId });
						
					userChallenge = service.Get (new GetUserChallenge {
						UserId = 1,
						ChallengeId = challengeId
					});
					Assert.That (DateTime.Now.ToLocalTime ().Subtract (userChallenge.AccessDate.FromEcmaScriptString ().ToLocalTime ()).TotalSeconds, Is.LessThan (10));
					Assert.That (userChallenge.HintAccess, Is.Null);
					Assert.That (userChallenge.SolvedDate, Is.Null);
					Assert.That (userChallenge.TryCount, Is.EqualTo (0));

					string firstChallengeAccess = userChallenge.AccessDate;

					//Access a hint
					using (var accessChallengeService = new ChallengesService { Request = new MockHttpRequest () })
						accessChallengeService.Get (new GetChallengeHint {
							Id = challengeId,
							HintNumber = 1
						});
					userChallenge = service.Get (new GetUserChallenge {
						UserId = 1,
						ChallengeId = challengeId
					});
					Assert.That (userChallenge.AccessDate, Is.EqualTo (firstChallengeAccess));
					Assert.That (userChallenge.HintAccess.Count, Is.EqualTo (1));
					Assert.That (DateTime.Now.ToLocalTime ().Subtract (userChallenge.HintAccess [0].AccessTime.FromEcmaScriptString ().ToLocalTime ()).TotalSeconds, Is.LessThan (10));
					Assert.That (userChallenge.HintAccess [0].HintId, Is.EqualTo (1));
					Assert.That (userChallenge.SolvedDate, Is.Null);
					Assert.That (userChallenge.TryCount, Is.EqualTo (0));

					HintAccessResponse firstHintAccess = userChallenge.HintAccess [0];

					//Post a bad solution
					try {
						challengeService.Post (new PostSolution {
							Id = challengeId,
							Solution = "gibberish"
						});
						Assert.Fail ("Posting a bad solution should fail.");
					} catch (HttpError e) {
						Assert.That (HttpStatusCode.BadRequest, Is.EqualTo (e.StatusCode));
					}
					userChallenge = service.Get (new GetUserChallenge {
						UserId = 1,
						ChallengeId = challengeId
					});
					Assert.That (userChallenge.AccessDate, Is.EqualTo (firstChallengeAccess));
					Assert.That (userChallenge.HintAccess.Count, Is.EqualTo (1));
					Assert.That (userChallenge.HintAccess [0], Is.EqualTo (firstHintAccess));
					Assert.That (userChallenge.SolvedDate, Is.Null);
					Assert.That (userChallenge.TryCount, Is.EqualTo (1));

					//Post a valid solution
					challengeService.Post (new PostSolution {
						Id = challengeId,
						Solution = "A solution"
					});
					userChallenge = service.Get (new GetUserChallenge {
						UserId = 1,
						ChallengeId = challengeId
					});
					Assert.That (userChallenge.AccessDate, Is.EqualTo (firstChallengeAccess));
					Assert.That (userChallenge.HintAccess.Count, Is.EqualTo (1));
					Assert.That (userChallenge.HintAccess [0], Is.EqualTo (firstHintAccess));
					Assert.That (DateTime.Now.ToLocalTime ().Subtract (userChallenge.SolvedDate.FromEcmaScriptString ().ToLocalTime ()).TotalSeconds, Is.LessThan (10));
					Assert.That (userChallenge.TryCount, Is.EqualTo (2));
				}

			}
		}

		[Test ()]
		public void GetUserChallengePointsIfSolvedNowTest ()
		{
			Mock<IPoints> pointsMock = new Mock<IPoints> ();
			DateTime solvedTimeMock = DateTime.MinValue;
			//Setup the mock, so it tracks the "solvedTime" parameter it was called with
			pointsMock.Setup (p => p.GetPoints (It.IsAny<Service> (), It.IsAny<int> (), It.IsAny<int> (), It.IsAny<DateTime> (), It.IsAny<DateTime> (), It.IsAny<IEnumerable<int>> ()))
				.Callback<Service,int,int,DateTime,DateTime, IEnumerable<int>> ((service, userId, challengeId, solvedTime, accessedTime, hints) => solvedTimeMock = solvedTime);
			appHost.Container.Register<IPoints> (pointsMock.Object);

			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					service.Get (new GetUserChallengePointsIfSolvedNow {
						ChallengeId = 1,
						UserId = 1
					});

					//The GetPoints method should have been called with the correct parameters
					pointsMock.Verify (p => p.GetPoints (service, 1, 1, It.IsAny<DateTime> (), It.IsAny<DateTime> (), It.IsAny <IEnumerable<int>> ()));
					Assert.That (DateTime.Now.Subtract (solvedTimeMock).TotalSeconds, Is.LessThan (1), "The solvedTime used to calculate the points should be very recent");
				}
			}
		}

		[Test ()]
		public void GetUserChallengePointsTest ()
		{
			int challengeId = 1;
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				//we have to store in the database, that the challenge is actually solved
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution {
					ChallengeId = 1,
					UserId = 1,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 123,
				});
				int points = service.Get (new GetUserChallengePoints {
					UserId = 1,
					ChallengeId = challengeId
				}).Points;
				Assert.That (points, Is.EqualTo (123));
			}
		}

		[Test ()]
		public void GetUserChallengePointsForceCalculationTest ()
		{
			Mock<IPoints> pointsMock = new Mock<IPoints> ();
			appHost.Container.Register<IPoints> (pointsMock.Object);

			int challengeId = 1;
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				//we have to store in the database, that the challenge is actually solved
				service.Db.Insert<ChallengeSolution> (new ChallengeSolution {
					ChallengeId = 1,
					UserId = 1,
					Solved = true,
					SolutionTime = DateTime.Now,
					Points = 100,
				});

				//Without setting the ForceCalculation parameter, the points should be read from the database and the point calculator should not be invoked
				int points = service.Get (new GetUserChallengePoints {
					UserId = 1,
					ChallengeId = challengeId
				}).Points;
				Assert.That (points, Is.EqualTo (100));
				pointsMock.Verify (p => p.GetPoints (service, 1, 1), Times.Never ());

				// Setting the ForceCalculation parameter to false should make the points be read from the database and the point calculator should not be invoked
				points = service.Get (new GetUserChallengePoints {
					UserId = 1,
					ChallengeId = challengeId,
					ForceCalculation = false,
				}).Points;
				Assert.That (points, Is.EqualTo (100));
				pointsMock.Verify (p => p.GetPoints (service, 1, 1), Times.Never ());

				// Setting the ForceCalculation parameter to false should make the points be read from the database and the point calculator should not be invoked
				points = service.Get (new GetUserChallengePoints {
					UserId = 1,
					ChallengeId = challengeId,
					ForceCalculation = true,
				}).Points;
				pointsMock.Verify (p => p.GetPoints (service, 1, 1), Times.Once ());
			}
		}


		[Test ()]
		public void GetChallengePointsAfterExpirationTest ()
		{
			using (var service = new UsersService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 4 });
				int challengeId = (int)service.Db.Insert<Challenge> (new Challenge (), selectIdentity: true);
				service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Id = 2,
					Challenges = new List<int>{ challengeId }
				});
				DateTime startDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0, 0));
				service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					EventId = 4,
					ChallengeSetId = 2,
					StartDate = startDate,
					EndDate = startDate.AddDays (1)
				});
				service.Db.Insert<EventUsers> (new EventUsers{ EventId = 4, UserId = 1 });
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					AccessTime = startDate.AddMinutes (1),
					ChallengeId = challengeId,
					Type = AccessType.Description,
					UserId = 1
				});
				service.Db.Insert (new ChallengeSolution {
					ChallengeId = challengeId,
					UserId = 1,
					Points = 99,
					SolutionTime = startDate.AddMinutes (2),
					Solved = true
				});

				// The points should still be readable after the challenge has expired
				int points = service.Get (new GetUserChallengePoints () {
					ChallengeId = challengeId,
					UserId = 1
				}).Points;
				Assert.That (points, Is.EqualTo (99));
			}
		}

	}
}

